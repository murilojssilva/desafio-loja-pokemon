import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import api from "../../services/api";
import Header from "../../components/Header";
import SideMenu from "../../components/SideMenu";

export default function Type() {
  let { id } = useParams();
  const [pokemons, setPokemons] = useState([]);
  const [type, setType] = useState("");
  const [name, setName] = React.useState();
  const [value, setValue] = useState(0);
  const [qtd, setQtd] = useState([]);
  useEffect(() => {
    async function fetchPokemons() {
      const response = await api.get(`/type/${id}`);
      setPokemons(response.data.pokemon);
      setType(response.data);
    }
    fetchPokemons();
  }, [id]);

  const results = !name
    ? pokemons
    : pokemons.filter((pokemon) =>
        pokemon.pokemon.name.toLowerCase().includes(name.toLocaleLowerCase())
      );
  useEffect(() => {
    setValue(qtd.reduce((subtotal, item) => subtotal + item[2], 0));
  }, [qtd]);
  function addItemToCart(qtd, index, pokemon) {
    let existingCartItem = qtd.find((item) => item[0] === index);
    if (existingCartItem) {
      let updatedCartItem = [
        existingCartItem[0],
        existingCartItem[1],
        existingCartItem[2] + (index + 1) * 4,
        existingCartItem[3] + 1,
      ];
      for (let i = 0; i < qtd.length; i++) {
        if (qtd[i] === existingCartItem) {
          qtd.splice(i, 1);
          i--;
        }
      }
      setQtd([...qtd, updatedCartItem]);
    } else {
      setQtd([...qtd, [index, pokemon.pokemon.name, (index + 1) * 4, 1]]);
      console.log(qtd);
    }
  }
  function removeItemToCart(qtd, index) {
    let existingCartItem = qtd.find((item) => item[0] === index);
    if (existingCartItem && existingCartItem[3]) {
      let updatedCartItem = [
        existingCartItem[0],
        existingCartItem[1],
        existingCartItem[2] - (index + 1) * 4,
        existingCartItem[3] - 1,
      ];
      for (let i = 0; i < qtd.length; i++) {
        if (qtd[i] === existingCartItem) {
          qtd.splice(i, 1);
          i--;
        }
      }
      if (existingCartItem[3] > 1) {
        setQtd([...qtd, updatedCartItem]);
      } else {
        setQtd([...qtd]);
      }
    }
  }
  return (
    <div className="content">
      {id < 19 ? (
        <>
          <Header />
          <SideMenu
            value={value}
            pokemons={pokemons}
            setValue={setValue}
            qtd={qtd}
            setQtd={setQtd}
          />
          <div className="type-title">
            <h1>Pokemons tipo {type.name}</h1>
            <input
              type="text"
              placeholder="Escreva o nome do pokemon desejado"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="list">
            {results.map((pokemon, index) => (
              <div className="pokemon" key={index}>
                <li key={pokemon.name}>
                  <img
                    src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.pokemon.url
                      .replace("https://pokeapi.co/api/v2/pokemon/", "")
                      .replace("/", ".png")}`}
                    alt={pokemon.pokemon.name}
                  />
                  <h3>{pokemon.pokemon.name}</h3>
                  <p>Valor: R$ {(index + 1) * 4},00</p>
                  <button
                    className="item-button"
                    onClick={() => addItemToCart(qtd, index, pokemon)}
                  >
                    +
                  </button>
                  <button
                    className="item-button"
                    onClick={() => removeItemToCart(qtd, index, pokemon)}
                  >
                    -
                  </button>
                </li>
              </div>
            ))}
          </div>
        </>
      ) : (
        <div className="not-found">
          <Header />
          <h1>Não há pokemons do tipo {type.name}</h1>
          <p>
            Voltar para <Link to="/">Página inicial</Link>
          </p>
        </div>
      )}
    </div>
  );
}
