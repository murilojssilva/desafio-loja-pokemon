import React, { useEffect, useState } from "react";
import api from "../../services/api";
import "./style.css";
import { Link } from "react-router-dom";
import Header from "../../components/Header";

export default function Main() {
  const [types, setTypes] = useState([]);
  useEffect(() => {
    async function fetchTypes() {
      const response = await api.get("/type/");
      setTypes(response.data.results);
    }
    fetchTypes();
  }, []);
  return (
    <div className="main">
      <Header />
      <div className="main-content">
        <div className="main-title">
          <h1>Loja Pokémon</h1>
          <h3>
            Bem-vindo a Pokestore. Selecione o tipo de pokemon que você deseja
            comprar.
          </h3>
        </div>
        {types.map((type, index) => (
          <div className="list-types" key={index}>
            {index + 1 === 19 || index + 1 === 20 ? (
              <div className="type-unknown"></div>
            ) : (
              <div className="type">
                <Link to={`/type/${index + 1}`}>
                  <img
                    src={require(`../../assets/img/types/${type.name}.png`)}
                    alt={type.name}
                  />{" "}
                  {type.name}
                </Link>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
}
