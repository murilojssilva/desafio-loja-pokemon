import React from "react";
import Header from "../../components/Header";
import { Link } from "react-router-dom";

export default function NotFound() {
  return (
    <div className="not-found">
      <Header />
      <h1>Game over</h1>
      <p>Essa página não existe</p>
      <p>
        Voltar para <Link to="/">Página inicial</Link>
      </p>
    </div>
  );
}
