import React, { useEffect, useState } from "react";
import axios from "axios";
import "./style.css";
import Header from "../../components/Header";

export default function History() {
  useEffect(() => {
    async function fetchHistory() {
      const response = await axios.get("http://localhost:3001/compras");
      setData(response.data);
    }

    fetchHistory();
  }, []);
  const [data, setData] = useState([]);
  return (
    <>
      <Header />{" "}
      <div className="history">
        <h1>Historico de compras</h1>
        {data.map((item) => (
          <>
            <p>
              <div className="history-item">
                {item.map((index, id) => (
                  <>
                    nome: {item[id][1]} valor: {item[id][2]} quantidade:{" "}
                    {item[id][3]}
                    <br />
                  </>
                ))}
              </div>
            </p>
          </>
        ))}
      </div>
    </>
  );
}
