import React from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import Main from "../pages/Main";
import History from "../pages/History";
import Type from "../pages/Type";
import NotFound from "../pages/NotFound";

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Main}></Route>
        <Route path="/history" exact component={History}></Route>
        <Route path="/type/:id" component={Type} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
}
