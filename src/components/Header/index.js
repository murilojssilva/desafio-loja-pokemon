import React from "react";
import { Link } from "react-router-dom";
import "./style.css";
import logo from "../../assets/img/pokeapi_256.png";

export default function Header() {
  return (
    <div className="header">
      <Link to="/">
        <img src={logo} alt="" />
      </Link>
      <Link to="/history">
        <p className="shopping">Minhas compras</p>
      </Link>
    </div>
  );
}
