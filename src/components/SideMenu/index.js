import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Modal from "react-modal";
import axios from "axios";
import "./style.css";
import trash from "../../assets/img/trash.svg";
import success from "../../assets/img/success.png";

function SideMenu(props) {
  const history = useHistory();
  const [modalIsOpen, setIsOpen] = useState(false);

  function closeModal() {
    props.setValue(0);
    props.setQtd([]);
    history.push("/");
    setIsOpen(false);
  }
  function handleSubmit() {
    if (props.value) {
      let jsonFormat = JSON.parse(JSON.stringify(props.qtd));
      axios.post("http://localhost:3001/compras", jsonFormat);
      setIsOpen(true);
    } else {
      alert("Não há itens no carrinho");
    }
  }
  function handleRemoveItem(index) {
    const temp = [...props.qtd];
    temp.splice(index, 1);
    props.setQtd(temp);
  }
  return (
    <div className="side-menu">
      <div className="title">
        <h2>Carrinho</h2>
      </div>
      <div className="side-menu-content">
        {props.qtd.map((item, index) => (
          <div className="item" key={index}>
            <p>
              {item[1]} R${item[2]},00 {item[3]} unidades{" "}
              <button
                className="remove-button"
                onClick={() => handleRemoveItem(index)}
              >
                <img src={trash} alt="Remover item" />
              </button>
            </p>
          </div>
        ))}
      </div>
      <div className="subtotal">
        <p>Valor total: R$ {props.value},00</p>
        <button className="finish-button" onClick={handleSubmit}>
          Finalizar compra
        </button>
      </div>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Submit Modal"
      >
        <div className="modal">
          <h2>Compra realizada com sucesso.</h2>
          <img src={success} alt="Compra realizada com sucesso." />
          <button className="close-button" onClick={closeModal}>
            x
          </button>
          <h3>Compra:</h3>
          <div className="purchase">
            {props.qtd.map((item, index) => (
              <>
                <p>
                  {item[1]}: R${item[2]},00 {item[3]} unidades
                </p>
              </>
            ))}

            <p>Total: R$ {props.value},00</p>
            <p>
              Você ganhou de volta: R${" "}
              {parseFloat((props.value * 0.05).toFixed(2))} (5%)
            </p>
          </div>
        </div>
      </Modal>
    </div>
  );
}

export default SideMenu;
