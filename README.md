## PokeStore App

Desafio proposto pela B2W Digital para a vaga de desenvolvedor júnior.

## Deploy da aplicaçação

https://bw2pokestore.netlify.app/

## Rodar aplicação em diretório local

Para rodar a aplicação em seu diretório, basta digitar:

### `yarn start`

para acessar no modo desenvolvedor<br />
Após isso, acesse http://localhost:3000 em seu navegador para acessar o webapp.

## Servidor

Para rodar a aplicação em seu diretório, basta digitar:

### `json-server --watch ./src/api/db.json --port 3001`

O servidor estará rodando na porta 3001
